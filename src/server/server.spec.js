import { addNewTask, updateTask } from './server';

(async function myFunc() {
    await addNewTask({
        name:"My task",
        id:"12349"
    });

    await updateTask({
        id:"12349",
        name:"My task - UPDATE"
    });
})();

