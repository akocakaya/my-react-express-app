import React from 'react';
import { connect } from 'react-redux';
import * as mutations from '../store/mutations';

class Login extends React.Component {
    
    render() {
        return (
            <div>
                <h2>
                    Please Login!
                </h2>
                <form onSubmit={this.props.mutations.authenticateUser}>
                    <input type="text" placeholder="username" name="username" defaultValue="Dev" />
                    <input type="password" placeholder="password" name="password" defaultValue="TUPLES" />
                    {this.props.authenticated === mutations.NOT_AUTHENTICATED ? <p> Login incorrect </p> : null}
                    <button type="submit" >Login</button>
                </form>
            </div>
        )
    }
}

function mapStateToProps({ session }) {
    return {
        authenticated: session.authenticated
    }
}

function mapDispatchToProps(dispatch) {
    return {
        mutations : {
            authenticateUser(e) {
                e.preventDefault();
                let username = e.target[`username`].value;
                let password = e.target[`password`].value;
                dispatch(mutations.requestAuthenticateUser(username, password));
            }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
