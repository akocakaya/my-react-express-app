import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as mutations from '../store/mutations';

class TaskDetail extends React.Component {

    render() {
        let p = this.props;
        let m = p.mutations;

        let taskName = p.task.name;
        let id = p.id;
        let isComplete = p.isComplete;
        let group = p.task.group;
        let groups = p.groups;

        return (
            <div>
                <div>
                    <input onChange={m.setTaskName} value={taskName} />
                </div>

                <div>
                    <button onClick={ () => m.setTaskCompletion(id, !isComplete) }>
                        {isComplete ? `Reopen` : 'Complete'}
                    </button>
                </div>

                <div>
                    <select onChange={m.setTaskGroup} value={group}>
                        {
                            groups.map(group => (
                                <option key={group.id} value={group.id}>
                                    {group.name}
                                </option>
                            ))
                        }
                    </select>
                </div>

                <div>
                    <Link to="/dashboard">
                        <button>
                            Done
                        </button>
                    </Link>
                </div>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch, ownProps) {
    const id = ownProps.match.params.id;

    return {
        mutations: {
            setTaskCompletion(id, isComplete) {
                dispatch(mutations.setTaskCompletion(id, isComplete));
            },
            setTaskGroup(e) {
                dispatch(mutations.setTaskGroup(id, e.target.value));
            },
            setTaskName(e) {
                dispatch(mutations.setTaskName(id, e.target.value));
            }
        }
    }
}

function mapStateToProps(state, ownProps) {
    let id = ownProps.match.params.id;
    let task = state.tasks.find(task => task.id == id);
    let groups = state.groups;

    return {
        id,
        task,
        groups,
        isComplete : task.isComplete
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskDetail);