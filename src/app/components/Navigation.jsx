import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class Navigation extends React.Component {
    
    render() {
        return (
            <div>
                <Link to="/dashboard">
                    <h1>
                        My React Express Application
                    </h1>
                </Link>
            </div>
        )
    }
}

export default connect(state => state)(Navigation);
