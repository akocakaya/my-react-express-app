import React from 'react';
import { Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { Redirect } from 'react-router';

import { history } from '../store/history';
import { store } from '../store';

import Login from './Login';
import Dashboard from './Dashboard';
import Navigation from './Navigation';
import TaskDetail from './TaskDetail';

const RouteGuard = Component => ({match}) => {
    if(!store.getState().session || !store.getState().session.authenticated) {
        return <Redirect to="/" />;
    } {
       return <Component match={match} />;
    }
}

class Main extends React.Component {

    render() {
        return (
            <Router history={history}>
                <Provider store={store}>
                    <div>
                        <Navigation />
                        <Route
                            exact
                            path="/"
                            component={Login}
                        />
                        <Route
                            exact
                            path="/dashboard"
                            render={ RouteGuard(Dashboard) }
                        />
                        <Route
                            exact
                            path="/task/:id"
                            render={ RouteGuard(TaskDetail) }
                        />
                    </div>
                </Provider>
            </Router>
        );
    }
}

export default Main;
