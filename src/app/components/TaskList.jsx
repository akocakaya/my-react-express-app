import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { requestTaskCreation } from '../store/mutations';

class TaskList extends React.Component {

    render() {
        return (
            <div>
                <h3>
                    {this.props.name}
                </h3>
                <div>
                    {
                        this.props.tasks.map(
                            task => (
                                <Link to={`/task/${task.id}`} key={ task.id }>
                                    <div>
                                        {task.name}
                                    </div>
                                </Link>
                            ) 
                        )
                    }
                </div>
                <button onClick={ () => this.props.mutations.createNewTask(this.props.id) }>
                    Add New Task
                </button>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let groupId = ownProps.id;
    return {
        name: ownProps.name,
        id  : groupId,
        tasks: state.tasks.filter(task => task.group == groupId)
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        mutations : {
            createNewTask(id) {
                console.log('createNewTask', id);
                dispatch(requestTaskCreation(id));
            }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskList)